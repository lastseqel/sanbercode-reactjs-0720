// // soal1

var kataPertama ="saya ";
var kataKedua ="senang "
var kataKetiga ="belajar "
var kataKeempat ="javascript"

console.log(kataPertama.concat(kataKedua.toUpperCase().substr(0 ,1).concat(kataKedua.substr(1).concat(kataKetiga.concat(kataKeempat.toUpperCase())))));

// soal2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var kataPertama =  Number("1");
var kataKedua = Number("2");
var kataKetiga = Number("3");
var kataKeempat = Number("4");
var hasil = kataPertama + kataKedua + kataKetiga + kataKeempat

console.log(hasil);

// soal3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = "javascript" ;
var kataKetiga =  "itu" ;
var kataKeempat =  "keren";
var kataKelima = "sekali";

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal4
var nilai = 78;
if (nilai >=80 && nilai <=100){
    console.log("Indeks = A");
}else if(nilai >= 70 && nilai < 80){
    console.log("Indeks = B");
}else if(nilai >= 60 && nilai < 70){
    console.log("Indeks = C");
}else if(nilai >= 50 && nilai < 60){
    console.log("Indeks = D");
}else if(nilai < 50){
    console.log("Indeks = E");
}else{
    console.log("Nilai tidak memenuhi syarat");
}

// soal5
var tanggal = 17;
var bulan = 7;
var tahun = 1997;

switch(bulan){
    case 1 :{console.log('Januari');break;}
    case 2 :{console.log('Februari');break;}
    case 3 :{console.log('Maret');break;}
    case 4 :{console.log('April');break;}
    case 5 :{console.log('Mei');break;}
    case 6 :{console.log('Juni');break;}
    case 7 :{console.log('Juli');break;}
    case 8 :{console.log('Agustus');break;}
    case 9 :{console.log('September');break;}
    case 10 :{console.log('Oktober');break;}
    case 11:{console.log('November');break;}
    case 12 :{console.log('Desember');break;}
}

console.log(tanggal + " " + bulan + " " + tahun);

