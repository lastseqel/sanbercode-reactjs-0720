// soal1
const newFunction = function literal(firstName, lastName){
  
    firstName = firstName
    lastName = lastName
    fullName ={firstName,lastName}
    return fullName
  
}

console.log(newFunction("William","Imoh"))

// soal2
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName,lastName,destination,occupation,spell} = newObject

  console.log(newObject)

//   soal3

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//Driver Code
let combined = [...west, ...east]
console.log(combined)