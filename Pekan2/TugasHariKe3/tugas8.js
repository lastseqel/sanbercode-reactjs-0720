// soal1
let luasLingkaran = (r) => {
    //function
    const phi = 3.14
    const hasil = phi * r * r 
    
    return hasil
    
} 
console.log(luasLingkaran(7));

// soal2
let kalimat =""
const gabung = (kata1 , kata2 ,kata3,kata4,kata5) =>{
    kata1 = "saya"
    kata2 = "adalah"
    kata3 = "seorang"
    kata4 = "frontend"
    kata5 = "developer"
    kalimat=`${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
    return kalimat
}

console.log(gabung())
// soal3
class Book {
    constructor(name,totalPage,price){
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}
    class Komik extends Book{
        constructor(name,totalPage,price){
            super()
            this.name = name
            this.totalPage = totalPage
            this.price = price
            this.isColorful = true
        }
        show(){
            return "nama buku "+ this.name+", total halaman "+this.totalPage+", harga buku "+this.price
        }
    }
    buku = new Komik("komik" , 10 ,10000)
    console.log(buku.show())